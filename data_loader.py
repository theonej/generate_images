import mxnet as mx
import numpy as np
from sklearn.preprocessing import normalize
from mxnet import io
from mxnet import image as mx_image
from mxnet import nd
from PIL import Image as pil_image
import os

image_replication_count = 1

input_dir_name = 'samples/abstract/'
output_dir_name = 'output/abstract/'

img_height = 1024
img_width = 1024

def load_sample(file_name):
    print(file_name)
    global input_dir_name

    truth_image = mx_image.imread(input_dir_name + file_name).astype(float)
    #sample_image = pil_image.fromarray(truth_image.asnumpy().astype('uint8'), 'RGB')
    #sample_image.save(output_dir_name + 'loaded_truth_image_0.jpg')

    resized = mx_image.imresize(truth_image, img_height, img_width)
    #sample_image = pil_image.fromarray(resized.asnumpy().astype('uint8'), 'RGB')
    #sample_image.save(output_dir_name + 'resized_truth_image_0.jpg')

    transposed =  nd.transpose(resized, (2, 0, 1))
    #print("transposed shape %s %s %s"%transposed.shape)
    #sample_image = pil_image.fromarray(transposed.asnumpy().astype('uint8'), 'RGB')
    #sample_image.save(output_dir_name + 'transposed_truth_image_0.jpg')


    #untransposed =  nd.transpose(transposed, (1, 2, 0))
    #print("untransposed shape %s %s %s"%untransposed.shape)
    #sample_image = pil_image.fromarray(untransposed.asnumpy().astype('uint8'), 'RGB')
    #sample_image.save(output_dir_name + 'untransposed_truth_image_0.jpg')
    
    scaled = (transposed / (255/2)) - 1

    return scaled

def get_truth_images_iter(batch_size):
    global input_dir_name
    paths = os.listdir(input_dir_name)

    total_image_count = len(paths) * image_replication_count
    batch_images = np.zeros((total_image_count, 3, img_height, img_width))
    
    for path_index in range(len(paths)):
        path = paths[path_index]

        sample = load_sample(path)

        #fake = np.clip((sample+1.0)*(255.0/2.0), 0, 255).astype(np.uint8)
        #save_sample(sample, 'truth_image_' + str(path_index) + '.jpg')

        for i in range(image_replication_count):
            image_index = (path_index * image_replication_count) + i

            batch_images[image_index] =  sample.asnumpy()[:,:,:]

        print("truth image batch shape: (%s, %s, %s, %s)"%batch_images.shape)

    print("creating iterator")
    image_iter = io.NDArrayIter(batch_images, batch_size=batch_size)

    return image_iter

def save_sample(sample, file_name):
    global output_dir_name

    nd_sample = nd.array(sample)
    scaled = ((nd_sample + 1)* (255 /2))
    transposed =  nd.transpose(scaled, (1, 2, 0)).astype('uint8')

    resized = mx_image.imresize(transposed, img_height, img_width)

    sample_image = pil_image.fromarray(resized.asnumpy(), 'RGB')
    sample_image.save(output_dir_name + file_name)