import mxnet as mx
import data_loader
from skimage.measure import compare_ssim as ssim
from RandIter import RandIter
from matplotlib import pyplot as plt
import numpy as np

ctx = mx.gpu() if mx.test_utils.list_gpus() else mx.cpu()

sigma = 0.02
learning_rate =.0002
beta = 0.5
no_bias = True

fix_gamma = True
epsilon = 1e-5 + 1e-12

batch_size =9
rand_iter = RandIter(batch_size, 100)
image_iter = data_loader.get_truth_images_iter(batch_size)

epochs = 10000

generator_params_file = 'generator'
discriminator_params_file = 'discriminator'

def create_generator():
    no_bias = True
    fix_gamma = True
    epsilon = 1e-5 + 1e-12

    rand = mx.sym.Variable('rand')

    
    g1 = mx.sym.Deconvolution(rand, name='g1', kernel=(4,4), num_filter=1024, no_bias=no_bias)
    gbn1 = mx.sym.BatchNorm(g1, name='gbn1', fix_gamma=fix_gamma, eps=epsilon)
    gact1 = mx.sym.Activation(gbn1, name='gact1', act_type='relu')

    g2 = mx.sym.Deconvolution(gact1, name='g2', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=512, no_bias=no_bias)
    gbn2 = mx.sym.BatchNorm(g2, name='gbn2', fix_gamma=fix_gamma, eps=epsilon)
    gact2 = mx.sym.Activation(gbn2, name='gact2', act_type='relu')

    g3 = mx.sym.Deconvolution(gact2, name='g3', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=256, no_bias=no_bias)
    gbn3 = mx.sym.BatchNorm(g3, name='gbn3', fix_gamma=fix_gamma, eps=epsilon)
    gact3 = mx.sym.Activation(gbn3, name='gact3', act_type='relu')

    g4 = mx.sym.Deconvolution(gact3, name='g4', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=128, no_bias=no_bias)
    gbn4 = mx.sym.BatchNorm(g4, name='gbn4', fix_gamma=fix_gamma, eps=epsilon)
    gact4 = mx.sym.Activation(gbn4, name='gact4', act_type='relu')

    g4_1 = mx.sym.Deconvolution(gact4, name='g4_1', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    gbn4_1 = mx.sym.BatchNorm(g4_1, name='gbn4_1', fix_gamma=fix_gamma, eps=epsilon)
    gact4_1 = mx.sym.Activation(gbn4_1, name='gact4_1', act_type='relu')

    #256
    g256 = mx.sym.Deconvolution(gact4_1, name='g256', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    gbn256 = mx.sym.BatchNorm(g256, name='gbn256', fix_gamma=fix_gamma, eps=epsilon)
    gact256 = mx.sym.Activation(gbn256, name='gact256', act_type='relu')

    #512
    g512 = mx.sym.Deconvolution(gact256, name='g512', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    gbn512 = mx.sym.BatchNorm(g512, name='gbn512', fix_gamma=fix_gamma, eps=epsilon)
    gact512 = mx.sym.Activation(gbn512, name='gact512', act_type='relu')

    #1024
    g1024 = mx.sym.Deconvolution(gact512, name='g1024', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    gbn1024 = mx.sym.BatchNorm(g1024, name='gbn1024', fix_gamma=fix_gamma, eps=epsilon)
    gact1024 = mx.sym.Activation(gbn1024, name='gact1024', act_type='relu')

    '''
    #2048
    g2048 = mx.sym.Deconvolution(gact1024, name='g2048', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    gbn2048 = mx.sym.BatchNorm(g2048, name='gbn2048', fix_gamma=fix_gamma, eps=epsilon)
    gact2048 = mx.sym.Activation(gbn2048, name='gact2048', act_type='relu')
    '''

    g5 = mx.sym.Deconvolution(gact1024, name='g5', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=3, no_bias=no_bias)
    generator_symbol = mx.sym.Activation(g5, name='gact5', act_type='tanh')

    

    generator = mx.mod.Module(symbol=generator_symbol, data_names=('rand',), label_names=None, context = ctx)
    #generator = mx.mod.Module.load(generator_params_file, 0)
   

    generator.bind(data_shapes=rand_iter.provide_data)
    generator.init_params(initializer=mx.init.Normal(sigma))
    generator.init_optimizer(optimizer='adam', optimizer_params = {'learning_rate':learning_rate, 'beta1':beta})
    
    
    sym, arg_params, aux_params = \
        mx.model.load_checkpoint(generator_params_file, 0)

    # assign parameters
    generator.set_params(arg_params, aux_params)
    

    return generator

def create_discriminator():
    data = mx.sym.Variable('data')

    '''
    #2048
    d2048 = mx.sym.Convolution(data, name='d2048', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=128, no_bias=no_bias)
    dact2048 = mx.sym.LeakyReLU(d2048, name='dact2048', act_type='leaky', slope=0.2)
    '''

    #1024
    d1024 = mx.sym.Convolution(data, name='d1024', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    #dbn1024 = mx.sym.BatchNorm(d1024, name='dbn1024', fix_gamma=fix_gamma, eps=epsilon)
    dact1024 = mx.sym.LeakyReLU(d1024, name='dact1024', act_type='leaky', slope=0.2)

    #512
    d512 = mx.sym.Convolution(dact1024, name='d512', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    dbn512 = mx.sym.BatchNorm(d512, name='dbn512', fix_gamma=fix_gamma, eps=epsilon)
    dact512 = mx.sym.LeakyReLU(dbn512, name='dact512', act_type='leaky', slope=0.2)

    #256
    d256 = mx.sym.Convolution(dact512, name='d256', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    dbn256 = mx.sym.BatchNorm(d256, name='dbn256', fix_gamma=fix_gamma, eps=epsilon)
    dact256 = mx.sym.LeakyReLU(dbn256, name='dact256', act_type='leaky', slope=0.2)

    #128
    d0 = mx.sym.Convolution(dact256, name='d0', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=64, no_bias=no_bias)
    dbn0 = mx.sym.BatchNorm(d0, name='dbn0', fix_gamma=fix_gamma, eps=epsilon)
    dact0 = mx.sym.LeakyReLU(dbn0, name='dact0', act_type='leaky', slope=0.2)

    d1 = mx.sym.Convolution(dact0, name='d1', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=128, no_bias=no_bias)
    dbn1 = mx.sym.BatchNorm(d1, name='dbn1', fix_gamma=fix_gamma, eps=epsilon)
    dact1 = mx.sym.LeakyReLU(dbn1, name='dact1', act_type='leaky', slope=0.2)

    d2 = mx.sym.Convolution(dact1, name='d2', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=256, no_bias=no_bias)
    dbn2 = mx.sym.BatchNorm(d2, name='dbn2', fix_gamma=fix_gamma, eps=epsilon)
    dact2 = mx.sym.LeakyReLU(dbn2, name='dact2', act_type='leaky', slope=0.2)

    d3 = mx.sym.Convolution(dact2, name='d3', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=512, no_bias=no_bias)
    dbn3 = mx.sym.BatchNorm(d3, name='dbn3', fix_gamma=fix_gamma, eps=epsilon)
    dact3 = mx.sym.LeakyReLU(dbn3, name='dact3', act_type='leaky', slope=0.2)

    d4 = mx.sym.Convolution(dact3, name='d4', kernel=(4,4), stride=(2,2), pad=(1,1), num_filter=1024, no_bias=no_bias)
    dbn4 = mx.sym.BatchNorm(d4, name='dbn4', fix_gamma=fix_gamma, eps=epsilon)
    dact4 = mx.sym.LeakyReLU(dbn4, name='dact4', act_type='leaky', slope=0.2)

    d5 = mx.sym.Convolution(dact4, name='d5', kernel=(4,4), num_filter=1, no_bias=no_bias)
    d5 = mx.sym.Flatten(d5)

    label = mx.sym.Variable('label')
    discriminator_symbol = mx.sym.LogisticRegressionOutput(data=d5, label=label, name='dloss')
    

    discriminator = mx.mod.Module(symbol=discriminator_symbol, data_names=('data',), label_names=('label',), context=ctx)
    
    discriminator.bind(data_shapes=image_iter.provide_data, label_shapes=[('label', (batch_size,))], inputs_need_grad=True)
    discriminator.init_params(initializer=mx.init.Normal(sigma))
    discriminator.init_optimizer(
        optimizer='adam', 
        optimizer_params={
            'learning_rate':learning_rate,
            'beta1':beta
    })
    
    
    sym, arg_params, aux_params = \
        mx.model.load_checkpoint(discriminator_params_file, 0)

    # assign parameters
    discriminator.set_params(arg_params, aux_params)
    

    return discriminator

def save_sample_images(samples):

    print("saving images with shape (%s, %s, %s)\n"%samples[0][0].shape)

    for index in range(len(samples[0])):
        item = samples[0][index]
        data_loader.save_sample(item.asnumpy(), str(index)+ '.jpg')

from matplotlib import pyplot as plt


print("creating generator\n")
generator = create_generator()
#generator.load_parameters(generator_params_file)

print("creating discriminator\n")
discriminator = create_discriminator()
#discriminator.load_parameters(discriminator_params_file)

for epoch in range(epochs):

    image_iter.reset()

    for i, batch in enumerate(image_iter):
        image_data = rand_iter.next()
        generator.forward(image_data, is_train=True)

        samples = generator.get_outputs()
        save_sample_images(samples)

        '''
            forward pass on the discriminator with the false images
        '''
        false_label = mx.nd.zeros((batch_size,), ctx=ctx)
        fake_batch = mx.io.DataBatch(samples, [false_label])

        discriminator.forward(fake_batch, is_train=True)
        discriminator.backward()
        false_gradient = [[grad.copyto(grad.context) for grad in grads] for grads in discriminator._exec_group.grad_arrays]


        '''
            forward pass on the discriminator with the false images
        '''
        true_label = false_label +1
        batch.label = [true_label]

        print(batch)
        discriminator.forward(batch, is_train=True)
        discriminator.backward()

        for true_grads, false_grads in zip(discriminator._exec_group.grad_arrays, false_gradient):
            for true_grad, false_grad in zip(true_grads, false_grads):
                true_grad += false_grad
        
        discriminator.update()

        '''
            update the gnerator with the  input gradients of the discriminator, in effect
            training the generator to be able to produce images that are likely to prodiuce a 
            prediction of true on the discriminator
        '''
        fake_batch.label = [true_label]
        discriminator.forward(fake_batch, is_train=True)
        discriminator.backward()

        discriminator_theta = discriminator.get_input_grads()
        generator.backward(discriminator_theta)
        generator.update()

        i += 1
        if i % 5 == 0:
            print("epoch:", epoch, "iter:", i)
            print("saving params")

            generator.save_checkpoint(generator_params_file, 0,)
            discriminator.save_checkpoint(discriminator_params_file, 0)
            #visualize(samples[0].asnumpy(), batch.data[0].asnumpy())
    

